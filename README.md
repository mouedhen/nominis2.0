# Nominis
> Solution pour la gestion des transactions immobilières au Québec

## Présentation

**Nominis** est une solution pour la gestion de la prospection des transactions immobilières au Québec (en un premiers temps).
**Nominis** vise les clients intéressés par le marché immobilier Canadien, ceci en leur offrant un outil facile à prendre en main répondant à leurs besoins avec une interface dynamique et intelligente qui leurs permettrait de consulter l’historique des transactions immobilières et les détails de chaque transaction.

## Liste des tâches

- [ ] US00 : En tant que développeur, je devrais pouvoir avoir un environnement de développement fiable
  - [x] Créer un compte gmail propre à l'application afin de regrouper les différents services
  - [x] Créer un compte Gitlab propre à l'organisation
  - [x] Créer un dépôt Git pour le partage du code
  - [ ] Créer une instance partagé de la base de données Firebase
  - [x] Installer les dépendances de base
  - [ ] Ajouter les membres de l'équipe aux différents services

- [ ] US01 : En tant qu'administrateur, je devrais pouvoir ajouter des utilisateurs.
  - [ ] Créer un système d'authentification classique sur Firebase
  - [ ] Créer le modèle utilisateur
  - [ ] Ajouter la logique d'ajout des utilisateurs
  - [ ] Ajouter la logique de listing des utilisateurs
  - [ ] Ajouter la logique de modification des utilisateurs
  - [ ] Ajouter la logique de suppression des utilisateurs
  - [ ] Créer une interface avec un formulaire pour l'ajout des utilisateurs
  - [ ] Créer une interface avec un tableau de tout les utilisateur

- [ ] US02 : En tant qu’administrateur, je devrais pouvoir définir le rôle de chaque utilisateur.
  - [ ] Ajouter la gestion des rôle à la base de données
  - [ ] Créer le modèle rôles
  - [ ] Ajouter la logique de listing des rôles
  - [ ] Ajouter un champ permettant la définition du rôle au formulaire utilisateur

- [ ] US03 : En tant qu’acteur, je devrais pouvoir me connecter à mon espace personnel.
  - [ ] Créer la page de connexion
  - [ ] Implémenter la logique d'authentification
  - [ ] Implémenter la logique de déconnexion

- [ ] US04 : En tant qu’acteur, je devrais pouvoir réinitialiser mon mot de passe en cas d’oubli.
  - [ ] Créer une page de récupération des mots de passe
  - [ ] Implémenter la logique pour la réinitialisation des mots de passe

- [ ] US05 : En tant qu’agent de collecte, je devrais pouvoir télécharger les documents que j’ai collectés sur le serveur.
  - [ ] Ajouter la prise en charge du stockage des fichiers à Firebase
  - [ ] Ajouter la gestion des documents à Firebase
  - [ ] Créer le modèle document
  - [ ] Ajouter la logique de téléchargement sur le serveur des documents
  - [ ] Créer une interface avec un formulaire permettant l'ajout de nouveaux documents
  - [ ] Limiter l'accès à l'ajout des documents aux utilisateurs avec le rôle de 'collecteur'

- [ ] US06 : En tant qu’agent de saisie, je devrais pouvoir consulter les documents collectés.
  - [ ] Ajouter la logique de listing des documents
  - [ ] Créer une interface avec un tableau contenant les documents téléchargé
  - [ ] Limiter l'accès au listing des documents aux utilisateurs avec le rôle de 'collecteur' et 'agent de saisie'

- [ ] US07 : En tant qu’agent de saisie, je devrais pouvoir savoir l’état des documents collectés (nouveaux / en cours traitement / traités).
  - [ ] Ajouter un champs statut à la table document
  - [ ] Ajouter un champs statut à l'interface de listing des documents

- [ ] US08 : En tant qu’agent de saisie, je devrais pouvoir saisir les informations se trouvant dans les différents document via un formulaire.
  - [ ] Ajouter la table actes à la base de données
  - [ ] Ajouter d'une interface avec un formulaire permettant l'ajout d'un nouvel acte
  - [ ] Créer le modèle acte
  - [ ] Ajouter la logique d'ajout d'un nouvel acte
  - [ ] Ajouter la logique de verrouillage des documents en cours de saisie lors du saisie des informations (document : nouveaux -> en cours de traitement)
  - [ ] Ajout de la logique d'archivage des documents saisie (document : en cours de traitement -> traité)

- [ ] US09 : En tant qu’agent de vérification, je devrais pouvoir consulter les actes saisis.
  - [ ] Créer une interface avec un tableau contenant les actes saisies
  - [ ] Créer une interface détails pour les actes
  - [ ] Ajouter la logique de listing des actes
  - [ ] Ajouter des filtres à l'interface de listing des actes
  - [ ] Limiter l'accès à la vue liste des actes aux utilisateurs avec le rôle de 'agent de saisie' ou 'agent de vérification'

- [ ] US10 : En tant qu’agent de vérification, je devrais pouvoir savoir l’état des actes saisis (nouveaux / vérifiés / publiés / archivés).
  - [ ] Ajouter un champs état à la table actes
  - [ ] Ajouter un champs état lors de l'affichage des actes
  - [ ] Ajouter la logique de changement de l'état d'un acte suivant le process métier exécuté

- [ ] US11 : En tant qu’agent de vérification, je devrais pouvoir corriger les erreurs de saisis des nouveaux actes.
  - [ ] Ajouter la logique pour la modification des actes saisies
  - [ ] Créer un historique des correction faîte sur les actes (pour des besoins ultérieurs)

- [ ] US12 : En tant que client, je devrais pouvoir consulter les actes publiés.
  - [ ] Créer l'interface client pour lister les actes

- [ ] US13 : En tant que client, je devrais pouvoir consulter les actualités journalières du marché immobilier sous forme d’une deporama.
  - [ ] Créer la table actualités dans la base de données
  - [ ] Créer le modèle actualité
  - [ ] Créer la logique d'ajout de l'actualité
  - [ ] Créer la logique de suppression de l'actualité
  - [ ] Créer une interface administrateur pour le saisie de l'actualité
  - [ ] Créer le composant graphique pour le défilement de l'actualité
  - [ ] Limiter l'accès à l'interface d'ajout de l'actualité aux utilisateurs avec le rôle de 'administrateur'
  - [ ] Générer un flux RSS pour le suivi de l'actualité

- [ ] US14 : En tant que client, je devrais pouvoir filtrer les actes publiés suivant plusieurs critères.
  - [ ] Ajout de filtres dynamiques à l'interface de listing des actes pour les clients

- [ ] US15 : En tant que client, je devrais pouvoir mettre en favoris les actes qui m’intéresse.
  - [ ] Créer la table favoris dans la base de données
  - [ ] Ajouter la logique pour l'ajout en favoris d'un acte
  - [ ] Ajouter les éléments graphiques nécessaires pour l'ajout en favoris d'un acte

- [ ] US16 : En tant que visiteur, je devrais pouvoir consulter les fonctionnalités offertes par la plateforme.
  - [ ] Créer une interface de présentation des différents prestations offerte par la plateforme

- [ ] US17 : En tant que visiteur, je devrais pouvoir m’inscrire à la plateforme.
  - [ ] Création d'une interface d'enregistrement des clients pour les différents services
