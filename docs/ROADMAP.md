# Nominis - Les recours fonciers +

## Feuille de route (v1.1)

- [CONFIG] Migration la base de données Neo4J

  - [ ] Adaptation de la conception à la base
  - [ ] Création des adaptateurs AbstractModel
  - [ ] Création du modèle utilisateur
  - [ ] Adaptation de la logique d'authentification
  - [ ] Adaptation des modèles métiers

- [FEATURE] Gestion des abonnements clients

  - [ ] Création du modèle type de transaction
  - [ ] Création du modèle région
  - [ ] Création du modèle circonscription
  - [ ] Création du modèle palier des transactions
  - [ ] Création du modèle durée des abonnements
  - [ ] Création du modèle bon de réductions
  - [ ] Création du modèle liste de prix transactions
  - [ ] Création du modèle liste de prix régions
  - [ ] Création du modèle liste de prix circonscription
  - [ ] Création du modèle liste de prix paliers des transactions
  - [ ] Création du modèle liste de prix durée des abonnements
  - [ ] Création de l'interface de gestion des types de transactions
  - [ ] Création de l'interface de gestion des régions
  - [ ] Création de l'interface de gestion des circonscriptions
  - [ ] Création de l'interface de gestion des paliers des transactions
  - [ ] Création de l'interface de gestion des durée des abonnements
  - [ ] Création de l'interface de gestion des bons de réduction

- [FEATURE] Facturation / Paiement

  - [ ] Création du modèle devis
  - [ ] Création du modèle facture
  - [ ] Ajout de la logique de l'application du bon de réduction au facture
  - [ ] Création de l'interface du choix des options de l'abonnement
  - [ ] Création de l'interface de consultation du devis
  - [ ] Création de l'API de paiement par paypal
  - [ ] Ajout de la logique de génération de la facture pour les paiements acceptés
  - [ ] Création de l'interface d'alerte pour les paiements refusés
  - [ ] Création de l'interface de consultation des factures
  - [ ] Ajout de la logique de génération de la facture en PDF
  - [ ] Ajout de la logique d'historisation des transactions clients
  - [ ] Ajout d'un composant pour la visualisation d' l'historique des transactions dans le tableau de bord de l'administrateur

- [FEATURE] Gestion des abonnements clients

  - [ ] Création de l'interface administrateur de visualisation des clients et le type de leurs abonnements, leurs factures et la date d'expiration
  - [ ] Marquage des clients d'après la date d'expiration de leurs abonnements (1 mois, 3 semaines, 2 semaines, 1 semaines)
  - [ ] Création de l'API d'envoi d'alertes au clients concernant leurs abonnements
  - [ ] Création d'une tâche planifiée pour l'envoie des e-mails d'alertes abonnements
  - [ ] Ajout de la logique de désactivation automatique des clients n'ayant pas renouvelé leurs abonnement
  - [ ] Ajout de la logique d'application de bon de réduction au factures clients / au nouveaux abonnées

- [FEATURE] Abonnement des clients

  - [ ] Ajout de la logique d'inscription des clients de manière autonome
  - [ ] Intégration de l'interface de choix des options de l'abonnement à la partie client
  - [ ] Ajout de la logique d'activation / désactivation de la période d'essai
