import moment from 'moment-timezone'

import AbstractModel from '../core/abstract-model'
import firestore from '../../../firestore'

moment.locale('fr')

class City extends AbstractModel {
  constructor ({id, category, city, office}) {
    super({id, category, city, office})
    this.config = {
      collection: firestore.collection('config').doc('configNominisResidentielle').collection('cities'),
      archiveCollection: firestore.collection('archives').doc(moment().format('LL')).collection('config').doc('configNominisResidentielle').collection('cities')
    }
  }

  serialize ({id, category, city, office}) {
    this.id = id !== undefined ? id : null
    this.category = category !== undefined ? category : null
    this.city = city !== undefined ? city : null
    this.office = office !== undefined ? city : null
    return this
  }
}

export default City
