import moment from 'moment-timezone'

import firestore from '../../../firestore'
import AbstractModel from '../core/abstract-model'

moment.locale('fr')

class Role extends AbstractModel {
  constructor ({id, designation, label}) {
    super({id, designation, label})
    this.config = {
      collection: firestore.collection('roles'),
      archiveCollection: firestore.collection('archives').doc(moment().format('LL')).collection('roles')
    }
  }

  serialize ({id, designation, label}) {
    this.id = id !== undefined ? id : null
    this.designation = designation !== undefined ? designation : null
    this.label = label !== undefined ? label : null
    return this
  }
}

export default Role
