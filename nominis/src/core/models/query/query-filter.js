import AbstractClass from '../core/abstract-class'

class QueryFilter extends AbstractClass {
  constructor ({params, operation, value}) {
    super({params, operation, value})
  }

  // @TODO validate operation string and throw error if not match
  serialize ({params, operation, value}) {
    this.params = params !== undefined ? params : null
    this.operation = operation !== undefined ? operation : null
    this.value = value !== undefined ? value : null
    return this
  }
}

export default QueryFilter
