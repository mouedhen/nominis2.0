import { cleanObject } from '../../helpers'

class AbstractClass {
  constructor (attributes) {
    this.serialize(attributes)
  }

  serialize (attributes) {
    return this
  }

  clean () {
    return cleanObject(this)
  }
}

export default AbstractClass
