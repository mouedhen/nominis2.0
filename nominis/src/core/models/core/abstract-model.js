import { cleanObject } from '../../helpers'

class AbstractModel {
  constructor (attributes) {
    this.serialize(attributes)
    this.config = {collection: undefined, archiveCollection: undefined}
    if (new.target === AbstractModel) {
      throw new TypeError('Cannot construct Abstract instances directly')
    }
  }

  createWithID (docID) {
    return this.config.collection.doc(docID).set({...this.clean()})
  }

  /**
   * Clear all object properties
   * @returns {*}
   */
  clearObject () {
    return this.serialize({})
  }

  serialize (attributes) {
    return this
  }

  /**
   * clean object for database manipulation
   * @returns {{}}
   */
  clean () {
    return cleanObject(this)
  }

  /**
   * Fetch all records from the database
   * @returns {Promise<Array>}
   */
  async fetchAll () {
    return this.config.collection.get()
      .then(snapshot => {
        let records = []
        snapshot.forEach(doc => {
          records.push({id: doc.id, ...doc.data()})
        })
        return records
      })
  }

  /**
   * Fetch record
   * @param id
   * @returns {Promise<*>}
   */
  async fetch (id) {
    return this.config.collection.doc(id).get()
      .then(doc => {
        this.serialize({id: doc.id, ...doc.data()})
        return this
      })
  }

  /**
   * Build query from filters
   * @param filters
   * @returns {undefined|*}
   */
  queryBuilder (filters) {
    filters = (filters !== undefined && Array.isArray(filters)) ? filters : []
    if (filters.length < 1) {
      return this.config.collection
    }
    let query = this.config.collection
    filters.forEach(x => {
      query = query.where(x.params, x.operation, x.value)
    })
    return query
  }

  /**
   * Fetch records from the database and handle filters
   * @param filters
   * @returns {Promise<Array>}
   */
  async filterQuery (filters) {
    return this.queryBuilder(filters).get()
      .then(snapshot => {
        let records = []
        snapshot.forEach(doc => {
          records.push({id: doc.id, ...doc.data()})
        })
        return records
      })
  }

  async fetchQuery (query) {
    return query.get()
      .then(snapshot => {
        let records = []
        snapshot.forEach(doc => {
          records.push({id: doc.id, ...doc.data()})
        })
        return records
      })
  }

  /**
   * Save record
   * @returns {Promise<*>}
   */
  async save () {
    if (this.id === undefined || this.id === null) {
      return this.create()
    }
    return this.update()
  }

  /**
   * Create record
   * @returns {Promise<*>}
   */
  async create () {
    return this.config.collection.doc().set({...this.clean()})
  }

  /**
   * Update record
   * @returns {Promise<*>}
   */
  async update () {
    return this.config.collection.doc(this.id).update({...this.clean()}).then(() => this)
  }

  /**
   * Delete the record from the database
   * @returns {Promise<void>}
   */
  async delete () {
    return this.config.collection.doc(this.id).delete()
  }

  /**
   * Archive record
   * @returns {Promise<*>}
   */
  async archive () {
    let archiveDoc = this
    return this.config.archiveCollection.add({...this.clean()})
      .then(() => {
        return archiveDoc.delete()
      })
  }
}

export default AbstractModel
