import AbstractClass from '../../core/abstract-class'

class GeoPoint extends AbstractClass {
  constructor ({latitude, longitude}) {
    super({latitude, longitude})
  }

  serialize ({latitude, longitude}) {
    this.latitude = latitude !== undefined ? latitude : null
    this.longitude = longitude !== undefined ? longitude : null
    return this
  }
}

export default GeoPoint
