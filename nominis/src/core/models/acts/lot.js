import firestore from '../../../firestore'
import moment from 'moment-timezone'

import AbstractModel from '../core/abstract-model'
import Act from './classes/act'
import UserAction from '../users/classes/user-action'

import { actActions, actStatus } from './enum/acts-status'
import { cleanObject, convertTimestamp2String } from '../../helpers'
import Address from '../locations/classes/address'
import Attachment from '../storage/classes/attachment'
import GeoPoint from '../locations/classes/geo-point'
import AbstractClass from '../core/abstract-class'

import eventBus from '../../../event-bus'
import firebase from '../../../firebase'
const favoritesCollection = firestore.collection('favorites')
// @TODO refract the code to more comprehensive process handler

class Lot extends AbstractModel {
  constructor (
    {
      id, act, lotNumber, office, category,
      registrationNumber, folderNumber, neighborhoodUnitNumber,
      address,
      constructionYear, fieldArea, buildingArea, floorsNumber, housingsNumber, commercialPremisesNumber,
      landValue, constructionValue, buildingValue, value,
      img, comment, resume, mapUrl, geoPoint,
      status,
      history,
      isPublished,
      locked
    }) {
    super({
      id,
      act,
      lotNumber,
      office,
      category,
      address,
      registrationNumber,
      folderNumber,
      neighborhoodUnitNumber,
      constructionYear,
      fieldArea,
      buildingArea,
      floorsNumber,
      housingsNumber,
      commercialPremisesNumber,
      landValue,
      constructionValue,
      buildingValue,
      value,
      img,
      comment,
      resume,
      mapUrl,
      geoPoint,
      status,
      history,
      isPublished,
      locked
    })
    this.config = {
      collection: firestore.collection('lots'),
      archiveCollection: firestore.collection('archives').doc(moment().format('LL')).collection('lots')
    }
    this.beingProcessedBy = this.getBeingProcessedBy()
    this.beingCorrectedBy = this.getBeingCorrectedBy()
    this.processor = this.getProcessor()
    this.corrector = this.getCorrector()
    this.publisher = this.getPublisher()
  }

  serialize (
    {
      id,
      act,
      lotNumber, office, category,
      address,
      registrationNumber, folderNumber, neighborhoodUnitNumber,
      constructionYear, fieldArea, buildingArea, floorsNumber, housingsNumber, commercialPremisesNumber,
      landValue, constructionValue, buildingValue, value,
      img, comment, resume, mapUrl, geoPoint,
      status,
      history,
      isPublished,
      locked
    }) {
    this.id = id === undefined ? null : id
    this.act = act === undefined ? new Act({}) : new Act({...act})
    this.lotNumber = lotNumber === undefined ? null : lotNumber
    this.office = office === undefined ? null : office
    this.category = category === undefined ? null : category
    this.address = address === undefined ? new Address({}) : new Address({...address})
    this.registrationNumber = registrationNumber === undefined ? null : registrationNumber
    this.folderNumber = folderNumber === undefined ? null : folderNumber
    this.neighborhoodUnitNumber = neighborhoodUnitNumber === undefined ? null : neighborhoodUnitNumber
    this.constructionYear = constructionYear === undefined ? null : constructionYear
    this.fieldArea = fieldArea === undefined ? null : fieldArea
    this.buildingArea = buildingArea === undefined ? null : buildingArea
    this.floorsNumber = floorsNumber === undefined ? null : floorsNumber
    this.housingsNumber = housingsNumber === undefined ? null : housingsNumber
    this.commercialPremisesNumber = commercialPremisesNumber === undefined ? null : commercialPremisesNumber
    this.landValue = landValue === undefined ? null : landValue
    this.constructionValue = constructionValue === undefined ? null : constructionValue
    this.buildingValue = buildingValue === undefined ? null : buildingValue
    this.value = value === undefined ? null : value
    this.img = img === undefined ? new Attachment({}) : new Attachment({...img})
    this.comment = comment === undefined ? null : comment
    this.resume = resume === undefined ? null : resume
    this.mapUrl = mapUrl === undefined ? null : mapUrl
    this.geoPoint = geoPoint === undefined ? new GeoPoint({}) : new GeoPoint({...geoPoint})
    this.status = status === undefined ? null : status
    this.history = history === undefined ? null : history
    this.isPublished = isPublished === undefined ? false : isPublished
    this.locked = locked === undefined ? false : locked
    return this
  }

  createNew ({attachment, user}) {
    this.act ? this.act.attachment = attachment : this.act = new Act({attachment})
    this.status = actStatus.NEW
    this.addToHistory({
      action: actActions.NEW,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    return super.save()
  }

  addToHistory (userAction) {
    if (!this.history) this.history = []
    this.history.push(new UserAction({...userAction}).clean())
    return this
  }

  clean () {
    this.act = this.act instanceof AbstractClass ? this.act.clean() : cleanObject(this.act)
    this.address = this.address instanceof AbstractClass ? this.address.clean() : cleanObject(this.address)
    this.img = this.img instanceof AbstractClass ? this.img.clean() : cleanObject(this.img)
    this.geoPoint = this.geoPoint instanceof AbstractClass ? this.geoPoint.clean() : cleanObject(this.geoPoint)
    return super.clean()
  }

  startProcess ({user}) {
    this.status = actStatus.BEING_PROCESSED
    this.addToHistory({
      action: actActions.BEING_PROCESSED,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    this.locked = true
    return super.save()
  }

  cancelProcess ({user}) {
    this.status = actStatus.NEW
    this.addToHistory({
      action: actActions.PROCESSING_CANCELED,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    this.locked = false
    return this.config.collection.doc(this.id).update({locked: false}).then(() => {
      return super.save()
    })
  }

  endProcess ({user}) {
    this.status = actStatus.PROCESSED
    this.addToHistory({
      action: actActions.PROCESSED,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    this.locked = false
    return this.config.collection.doc(this.id).update({locked: false}).then(() => {
      return super.save()
    })
  }

  startCorrection ({user}) {
    this.locked = true
    this.status = actStatus.BEING_CORRECTED
    this.addToHistory({
      action: actActions.BEING_CORRECTED,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    return super.save()
  }

  cancelCorrection ({user}) {
    this.status = actStatus.PROCESSED
    this.addToHistory({
      action: actActions.CORRECTION_CANCELED,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    this.locked = false
    return this.config.collection.doc(this.id).update({locked: false}).then(() => {
      return super.save()
    })
  }

  endCorrection ({user}) {
    this.status = actStatus.CORRECTED
    this.addToHistory({
      action: actActions.CORRECTED,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    this.locked = false
    return this.config.collection.doc(this.id).update({locked: false}).then(() => {
      return super.save()
    })
  }

  returnToOfficier ({user}) {
    this.status = actStatus.RETURNED
    this.addToHistory({
      action: actActions.RETURNED,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    this.locked = false
    return this.config.collection.doc(this.id).update({locked: false}).then(() => {
      return super.save()
    })
  }

  lock () {
    let lot = new Lot({...this})
    lot.config.collection.doc(lot.id).update({locked: this.locked})
  }
  publish ({user}) {
    this.status = actStatus.PUBLISHED
    this.isPublished = true
    this.addToHistory({
      action: actActions.PUBLISHED,
      date: new Date(),
      user: {
        uid: user.uid,
        firstName: user.firstName !== undefined ? user.firstName : '',
        lastName: user.lastName !== undefined ? user.lastName : (user.firstName === undefined ? user.email.slice(0, user.email.lastIndexOf('@')) : '')
      }
    })
    this.locked = false
    return this.config.collection.doc(this.id).update({locked: false}).then(() => {
      return super.save().then(record => {
        eventBus.publish('publish-lot', this)
        return record
      })
        .catch(e => {
          console.log(e)
        })
    })
  }

  async delete () {
    let lotID = this.id
    let status = this.status
    if (this.act.attachment.name) {
      eventBus.publish('delete-attachment', this.act.attachment.name)
    }
    return super.delete().then((result) => {
      if (status === actStatus.PUBLISHED) {
        eventBus.publish('unpublish-lot', lotID)
        return result
      }
    })
  }

  async update () {
    let lotID = this.id
    let status = this.status
    return super.update().then(result => {
      if (status !== actStatus.PUBLISHED) {
        eventBus.publish('unpublish-lot', lotID)
        return result
      }
      return result
    })
  }

  getProcessor () {
    let editor = {}
    if (this.status === actStatus.PROCESSED || this.status === actStatus.BEING_CORRECTED || this.status === actStatus.CORRECTED ||
      this.status === actStatus.RETURNED || this.status === actStatus.PUBLISHED || this.status === actStatus.ARCHIVED) {
      let inversedHistory = this.history.slice().reverse()
      inversedHistory.some(action => {
        if (action.action === actActions.PROCESSED) {
          editor = {uid: action.user.uid, firstName: action.user.firstName, lastName: action.user.lastName, actionDate: convertTimestamp2String(action.date), actionDescription: action.user.firstName + ' ' + action.user.lastName + ' - ' + convertTimestamp2String(action.date)}
          return true
        }
      })
    }
    return editor
  }

  getCorrector () {
    let corrector = {}
    if (this.status === actStatus.CORRECTED || this.status === actStatus.PUBLISHED || this.status === actStatus.ARCHIVED) {
      let inversedHistory = this.history.slice().reverse()
      inversedHistory.some(action => {
        if (action.action === actActions.CORRECTED) {
          corrector = {uid: action.user.uid, firstName: action.user.firstName, lastName: action.user.lastName, actionDate: convertTimestamp2String(action.date), actionDescription: action.user.firstName + ' ' + action.user.lastName + ' - ' + convertTimestamp2String(action.date)}
          return true
        }
      })
    }
    return corrector
  }

  getPublisher () {
    let publisher = {}
    if (this.status === actStatus.PUBLISHED || this.status === actStatus.ARCHIVED) {
      let inversedHistory = this.history.slice().reverse()
      inversedHistory.some(action => {
        if (action.action === actActions.PUBLISHED) {
          publisher = {uid: action.user.uid, firstName: action.user.firstName, lastName: action.user.lastName, actionDate: convertTimestamp2String(action.date), actionDescription: action.user.firstName + ' ' + action.user.lastName + ' - ' + convertTimestamp2String(action.date)}
          return true
        }
      })
    }
    return publisher
  }

  getBeingProcessedBy () {
    let processor = {}
    if (this.status === actStatus.BEING_PROCESSED) {
      let inversedHistory = this.history.slice().reverse()
      inversedHistory.some(action => {
        if (action.action === actActions.BEING_PROCESSED) {
          processor = {uid: action.user.uid, firstName: action.user.firstName, lastName: action.user.lastName, actionDate: convertTimestamp2String(action.date), actionDescription: action.user.firstName + ' ' + action.user.lastName + ' - ' + convertTimestamp2String(action.date)}
          return true
        }
      })
    }
    return processor
  }

  getBeingCorrectedBy () {
    let corrector = {}
    if (this.status === actStatus.BEING_CORRECTED) {
      let inversedHistory = this.history.slice().reverse()
      inversedHistory.some(action => {
        if (action.action === actActions.BEING_CORRECTED) {
          corrector = {uid: action.user.uid, firstName: action.user.firstName, lastName: action.user.lastName, actionDate: convertTimestamp2String(action.date), actionDescription: action.user.firstName + ' ' + action.user.lastName + ' - ' + convertTimestamp2String(action.date)}
          return true
        }
      })
    }
    return corrector
  }

  async addToFavorites () {
    return favoritesCollection.doc().set({lotId: this.id, userId: firebase.auth().currentUser.uid})
  }

  async removeFromFavorites (favId) {
    return favoritesCollection.doc(favId).delete()
  }
}

export default Lot
