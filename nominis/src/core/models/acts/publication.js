import firestore from '../../../firestore'
import AbstractModel from '../core/abstract-model'
import Act from './classes/act'
import { cleanObject } from '../../helpers'
import Address from '../locations/classes/address'
import Attachment from '../storage/classes/attachment'
import GeoPoint from '../locations/classes/geo-point'
import AbstractClass from '../core/abstract-class'
import firebase from '../../../firebase'
import moment from 'moment-timezone'
const favoritesCollection = firestore.collection('favorites')
class Publication extends AbstractModel {
  constructor (
    {
      id, act, lotNumber, office, category,
      registrationNumber, folderNumber, neighborhoodUnitNumber,
      address,
      constructionYear, fieldArea, buildingArea, floorsNumber, housingsNumber, commercialPremisesNumber,
      landValue, constructionValue, buildingValue, value,
      img, comment, resume, mapUrl, geoPoint
    }) {
    super({
      id,
      act,
      lotNumber,
      office,
      category,
      address,
      registrationNumber,
      folderNumber,
      neighborhoodUnitNumber,
      constructionYear,
      fieldArea,
      buildingArea,
      floorsNumber,
      housingsNumber,
      commercialPremisesNumber,
      landValue,
      constructionValue,
      buildingValue,
      value,
      img,
      comment,
      resume,
      mapUrl,
      geoPoint
    })
    this.config = {
      collection: firestore.collection('publications'),
      archiveCollection: firestore.collection('archives').doc(moment().format('LL')).collection('publications')
    }
  }

  serialize (
    {
      id,
      act,
      lotNumber, office, category,
      address,
      registrationNumber, folderNumber, neighborhoodUnitNumber,
      constructionYear, fieldArea, buildingArea, floorsNumber, housingsNumber, commercialPremisesNumber,
      landValue, constructionValue, buildingValue, value,
      img, comment, resume, mapUrl, geoPoint
    }) {
    this.id = id === undefined ? null : id
    this.act = act === undefined ? new Act({}) : new Act({...act})
    this.lotNumber = lotNumber === undefined ? null : lotNumber
    this.office = office === undefined ? null : office
    this.category = category === undefined ? null : category
    this.address = address === undefined ? new Address({}) : new Address({...address})
    this.registrationNumber = registrationNumber === undefined ? null : registrationNumber
    this.folderNumber = folderNumber === undefined ? null : folderNumber
    this.neighborhoodUnitNumber = neighborhoodUnitNumber === undefined ? null : neighborhoodUnitNumber
    this.constructionYear = constructionYear === undefined ? null : constructionYear
    this.fieldArea = fieldArea === undefined ? null : fieldArea
    this.buildingArea = buildingArea === undefined ? null : buildingArea
    this.floorsNumber = floorsNumber === undefined ? null : floorsNumber
    this.housingsNumber = housingsNumber === undefined ? null : housingsNumber
    this.commercialPremisesNumber = commercialPremisesNumber === undefined ? null : commercialPremisesNumber
    this.landValue = landValue === undefined ? null : landValue
    this.constructionValue = constructionValue === undefined ? null : constructionValue
    this.buildingValue = buildingValue === undefined ? null : buildingValue
    this.value = value === undefined ? null : value
    this.img = img === undefined ? new Attachment({}) : new Attachment({...img})
    this.comment = comment === undefined ? null : comment
    this.resume = resume === undefined ? null : resume
    this.mapUrl = mapUrl === undefined ? null : mapUrl
    this.geoPoint = geoPoint === undefined ? new GeoPoint({}) : new GeoPoint({...geoPoint})
    return this
  }

  clean () {
    this.act = this.act instanceof AbstractClass ? this.act.clean() : cleanObject(this.act)
    this.address = this.address instanceof AbstractClass ? this.address.clean() : cleanObject(this.address)
    this.img = this.img instanceof AbstractClass ? this.img.clean() : cleanObject(this.img)
    this.geoPoint = this.geoPoint instanceof AbstractClass ? this.geoPoint.clean() : cleanObject(this.geoPoint)
    return super.clean()
  }

  async addToFavorites () {
    return favoritesCollection.doc().set({lotId: this.id, userId: firebase.auth().currentUser.uid})
  }

  async removeFromFavorites (favId) {
    return favoritesCollection.doc(favId).delete()
  }
}

export default Publication
