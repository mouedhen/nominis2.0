import AbstractClass from '../../core/abstract-class'

import Attachment from '../../storage/classes/attachment'
import Owner from './owner'
import Seller from './seller'
import Jurist from './jurist'
import { cleanObject, convertTimestamp2String } from '../../../helpers'

class Act extends AbstractClass {
  constructor ({attachment, inscriptionNumber, inscriptionDate, transactionType, deadline, owner, seller, jurist}) {
    super({attachment, inscriptionNumber, inscriptionDate, transactionType, deadline, owner, seller, jurist})
    this.setDataForDisplay(inscriptionDate, deadline, owner, seller, jurist)
  }

  serialize ({attachment, inscriptionNumber, inscriptionDate, transactionType, deadline, owner, seller, jurist}) {
    this.attachment = attachment !== undefined ? new Attachment({...attachment}) : new Attachment({})
    this.inscriptionNumber = inscriptionNumber !== undefined ? inscriptionNumber : null
    this.inscriptionDate = inscriptionDate !== undefined ? inscriptionDate : null
    this.transactionType = transactionType !== undefined ? transactionType : null
    this.deadline = deadline !== undefined ? deadline : null
    this.owner = owner !== undefined ? new Owner({...owner}) : new Owner({})
    this.seller = seller !== undefined ? new Seller({...seller}) : new Seller({})
    this.jurist = jurist !== undefined ? new Jurist({...jurist}) : new Jurist({})
  }

  clean () {
    this.attachment = cleanObject(this.attachment)
    this.owner = cleanObject(this.owner)
    this.seller = cleanObject(this.seller)
    this.jurist = cleanObject(this.jurist)
    return super.clean()
  }

  setDataForDisplay (inscriptionDate, deadline, owner, seller, jurist) {
    this.inscriptionDateDisplay = convertTimestamp2String(inscriptionDate)
    this.deadlineDisplay = convertTimestamp2String(deadline)
    if (owner) {
      if (owner.name && owner.category) {
        this.ownerDisplay = owner.category.charAt(0).toUpperCase() + owner.category.slice(1) + ' : ' + owner.name
      }
    }
    if (seller) {
      if (seller.name && seller.category) {
        this.sellerDisplay = seller.category.charAt(0).toUpperCase() + seller.category.slice(1) + ' : ' + seller.name
      }
    }
    if (jurist) {
      if (jurist.name && jurist.category) {
        this.juristDisplay = jurist.category.charAt(0).toUpperCase() + jurist.category.slice(1) + ' : ' + jurist.name
      }
    }
  }
}

export default Act
