import PDFJS from 'pdfjs-dist'

PDFJS.workerSrc = require('pdfjs-dist/build/pdf.worker')

/**
 * Parse a pdf file into a text
 * @author https://ourcodeworld.com/articles/read/405/how-to-convert-pdf-to-text-extract-text-from-pdf-with-javascript
 */
class PDFParser {
  constructor ({data}) {
    this.data = data !== undefined ? data : null
  }

  /**
   * Get full text from pdf doc
   * @returns {Promise<any>}
   */
  getFullText () {
    return new Promise((resolve, reject) => {
      PDFJS.getDocument(this.data)
        .then(pdf => {
          let pagesPromise = []
          for (let i = 0; i < pdf.pdfInfo.numPages; i++) {
            (pageNumber => {
              pagesPromise.push(this.getPageText(pageNumber, pdf))
            })(i + 1)
          }
          let pdfText = ''
          Promise.all(pagesPromise).then(pageText => {
            pdfText += pageText + ' '
            resolve(pdfText)
          })
        })
    })
  }

  /**
   * Retrieves the text of a specif page within a PDF Document obtained through pdf.js
   * @param {Integer} pageNumber
   * @param {PDFDocument} PDFDocumentInstance
   * @returns {Promise<any>}
   */
  getPageText (pageNumber, PDFDocumentInstance) {
    return new Promise((resolve, reject) => {
      PDFDocumentInstance.getPage(pageNumber)
        .then(pdfPage => {
          pdfPage.getTextContent()
            .then(textContent => {
              let textItems = textContent.items
              let finalString = ''
              for (let i = 0; i < textItems.length; i++) {
                finalString += textItems[i].str + ' '
              }
              resolve(finalString)
            })
        })
    })
  }
}

export default PDFParser
