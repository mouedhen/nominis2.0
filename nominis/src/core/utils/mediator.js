import PubSub from './pub-sub'

/*
 * Mediator Pattern
 */
export default class Mediator extends PubSub {
  constructor (opts) {
    return super()
  }

  attachToObject (obj) {
    obj.handlers = []
    obj.publish = this.publish
    obj.subscribe = this.subscribe
  }
}
