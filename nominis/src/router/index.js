import Vue from 'vue'
import Router from 'vue-router'

import Tests from '../views/Tests'

import PublicIndex from '../views/public/PublicIndex'

import SignIn from '../views/auth/SignIn'

import DashboardIndex from '../views/dashboard/DashboardIndex'

import NewsIndex from '../views/dashboard/news/NewsIndex'

import LotsIndex from '../views/dashboard/acts/LotsIndex'
import LotsVerificationIndex from '../views/dashboard/acts/LotsVerificationIndex'
import LotsPublicationIndex from '../views/dashboard/acts/LotsPublicationIndex'
import LotForm from '../views/dashboard/acts/LotForm'
import LotDetails from '../views/dashboard/acts/LotDetails'

import FavoritesIndex from '../views/dashboard/customers/FavoritesIndex'
import PurchasesIndex from '../views/dashboard/customers/PurchasesIndex'
import CustomersIndex from '../views/dashboard/customers/CustomersIndex'
import CustomersForm from '../views/dashboard/customers/CustomersForm'
import Publications from '../views/dashboard/customers/Publications'
import PublicationsDetails from '../views/dashboard/customers/PublicationsDetails'

import UsersIndex from '../views/dashboard/users/UsersIndex'
import UserForm from '../views/dashboard/users/UserForm'
import NewsForm from '../views/dashboard/news/NewsForm'

Vue.use(Router)
let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: PublicIndex
    },
    {
      path: '/tests',
      name: 'tests',
      component: Tests
    },
    {
      path: '/sign-in',
      name: 'sign-in',
      component: SignIn
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardIndex
    },
    {
      path: '/dashboard/lots',
      name: 'lots-index',
      component: LotsIndex
    },
    {
      path: '/dashboard/lots/process/:id/:action?',
      name: 'lot-process',
      component: LotForm
    },
    {
      path: '/dashboard/lots/details/:id/',
      name: 'lot-details',
      component: LotDetails
    },
    {
      path: '/dashboard/lots/verifications',
      name: 'lots-verifications',
      component: LotsVerificationIndex
    },
    {
      path: '/dashboard/lots/publications',
      name: 'lots-publications',
      component: LotsPublicationIndex
    },
    {
      path: '/dashboard/users',
      name: 'users-index',
      component: UsersIndex
    },
    {
      path: '/dashboard/users/create',
      name: 'create-user',
      component: UserForm
    },
    {
      path: '/dashboard/users/:uid/update',
      name: 'update-user',
      component: UserForm
    },
    {
      path: '/dashboard/customers/create',
      name: 'create-customer',
      component: CustomersForm
    },
    {
      path: '/dashboard/news/:id/create',
      name: 'create-news',
      component: NewsForm
    },
    {
      path: '/dashboard/customers/:uid/update',
      name: 'update-customer',
      component: CustomersForm
    },
    {
      path: '/dashboard/customers',
      name: 'customers-index',
      component: CustomersIndex
    },
    {
      path: '/dashboard/favorites',
      name: 'favorites-index',
      component: FavoritesIndex
    },
    {
      path: '/dashboard/acts',
      name: 'purchases-index',
      component: PurchasesIndex
    },
    {
      path: '/dashboard/news',
      name: 'news-index',
      component: NewsIndex
    },
    {
      path: '/dashboard/publications',
      name: 'publications',
      component: Publications
    },
    {
      path: '/dashboard/publications/details',
      name: 'publications-details',
      component: PublicationsDetails
    }
  ]
})

export default router
