import { INIT, FETCH_ALL, FETCH, CREATE, UPDATE, DELETE, LOGIN, LOGIN_CUSTOMER, LOGOUT } from './mutations-types'
import User from '../../core/models/users/user'
import Customer from '../../core/models/customers/Customer'

export const mutations = {
  [INIT] (state) {
    state.all = []
  },

  [FETCH_ALL] (state, records) {
    state.all = records
  },

  [FETCH] (state, record) {
    const index = state.all.findIndex(x => x.uid === record.uid)
    if (index === -1) {
      state.all.push(record)
    } else {
      state.all.splice(index, 1, record)
    }
  },

  [CREATE] (state, record) {
    const index = state.all.findIndex(x => x.uid === record.uid)
    if (index === -1) {
      state.all.push(record)
    }
  },

  [UPDATE] (state, record) {
    const index = state.all.findIndex(x => x.uid === record.uid)
    if (index !== -1) {
      state.all.splice(index, 1, record)
    }
  },

  [DELETE] (state, record) {
    state.all = state.all.filter(x => x.uid !== record.uid)
  },

  [LOGIN] (state, user) {
    state.user = user
  },

  [LOGIN_CUSTOMER] (state, customer) {
    state.customer = customer
  },

  [LOGOUT] (state) {
    state.user = new User({})
    state.customer = new Customer({})
  }
}
