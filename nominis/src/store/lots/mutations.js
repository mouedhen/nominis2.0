import { INIT, FETCH_ALL, FETCH, CREATE, UPDATE, DELETE, CREATE_VERIFICATION, UPDATE_VERIFICATION, DELETE_VERIFICATION, CREATE_ENTRY, UPDATE_ENTRY, DELETE_ENTRY, CREATE_PUBLICATION, UPDATE_PUBLICATION, DELETE_PUBLICATION } from './mutations-types'

export const mutations = {
  [INIT] (state) {
    state.all = []
  },

  [FETCH_ALL] (state, records) {
    state.all = records
  },

  [FETCH] (state, record) {
    const index = state.all.findIndex(x => x.id === record.id)
    if (index === -1) {
      state.all.push(record)
    } else {
      state.all.splice(index, 1, record)
    }
  },

  [CREATE] (state, record) {
    const index = state.all.findIndex(x => x.id === record.id)
    if (index === -1) {
      state.all.push(record)
    }
  },

  [UPDATE] (state, record) {
    const index = state.all.findIndex(x => x.id === record.id)
    if (index !== -1) {
      state.all.splice(index, 1, record)
    }
  },

  [DELETE] (state, record) {
    state.all = state.all.filter(x => x.id !== record.id)
  },

  [CREATE_VERIFICATION] (state, record) {
    const index = state.verification.findIndex(x => x.id === record.id)
    if (index === -1) {
      state.verification.push(record)
    }
  },

  [UPDATE_VERIFICATION] (state, record) {
    const index = state.verification.findIndex(x => x.id === record.id)
    if (index !== -1) {
      state.verification.splice(index, 1, record)
    }
  },

  [DELETE_VERIFICATION] (state, record) {
    state.verification = state.verification.filter(x => x.id !== record.id)
  },

  [CREATE_ENTRY] (state, record) {
    const index = state.entry.findIndex(x => x.id === record.id)
    if (index === -1) {
      state.entry.push(record)
    }
  },

  [UPDATE_ENTRY] (state, record) {
    const index = state.entry.findIndex(x => x.id === record.id)
    if (index !== -1) {
      state.entry.splice(index, 1, record)
    }
  },

  [DELETE_ENTRY] (state, record) {
    state.entry = state.entry.filter(x => x.id !== record.id)
  },

  [CREATE_PUBLICATION] (state, record) {
    const index = state.publication.findIndex(x => x.id === record.id)
    if (index === -1) {
      state.publication.push(record)
    }
  },

  [UPDATE_PUBLICATION] (state, record) {
    const index = state.publication.findIndex(x => x.id === record.id)
    if (index !== -1) {
      state.publication.splice(index, 1, record)
    }
  },

  [DELETE_PUBLICATION] (state, record) {
    state.publication = state.publication.filter(x => x.id !== record.id)
  }
}
