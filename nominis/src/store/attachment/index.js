import firestore from '../../firestore'
import Attachment from '../../models/acts/attachement'

// import { FETCH_ALL, FETCH, CREATE, UPDATE, DELETE } from '../common/mutations-types'
import { INIT, FETCH_ALL, CREATE, UPDATE, DELETE } from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'

const actions = {
  async fetchAll (context) {
    (new Attachment({})).fetchAll()
      .then(attachments => {
        context.commit(FETCH_ALL, attachments)
      })
      .catch(error => {
        console.log(error)
      })
  },

  /**
   * Synchronize Firestore with Vuex store
   * @param context
   * @returns {Promise<void>}
   */
  async syncCollection (context) {
    context.commit(INIT)
    firestore.collection('attachments').onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new Attachment({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new Attachment({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new Attachment({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new Attachment({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new Attachment({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  },

  async fetchQuery (context, filters) {
    (new Attachment({})).fetchQuery(filters)
      .then(attachments => {
        context.commit(FETCH_ALL, attachments)
      })
      .catch(error => {
        console.log(error)
      })
  },

  async create (context, attachment) {
    (new Attachment(attachment)).create()
      .then(attachment => {
        context.commit(CREATE, attachment)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
