import AbstractModel from '../core/abstract-model'
import firestore from '../../../firestore'

class Category extends AbstractModel {
  constructor ({id, name}) {
    super()
    this.serialize({id, name})
    this.config = {
      collection: firestore.collection('config').doc('configNominisResidentielle').collection('categories'),
      archiveCollection: firestore.collection('archives').doc('nominisArchives').collection('config').doc('configNominisResidentielle').collection('categories')
    }
  }

  serialize ({id, name}) {
    this.id = id
    this.name = name
  }
}

export default Category
