import { docStatus, actsCollection, usersCollection, favoritesCollection } from '../core/helpers'
import firebase from 'firebase'

/**
 * @deprecated
 */
class ActDocument {
  constructor ({id, title, file, status}) {
    this.id = id
    this.title = title
    this.file = file
    if (status) {
      this.status = status
    } else {
      this.status = docStatus.NEW
    }
  }

  setFile ({name, type, url}) {
    this.file = {name, type, url}
  }

  async save () {
    return actsCollection.add({title: this.title, file: this.file, status: this.status})
  }

  getObject () {
    return ({
      title: this.title,
      file: this.file,
      status: this.status
    })
  }

  async get () {
    return actsCollection.get().then(snapshot => {
      var acts = []
      snapshot.forEach(doc => {
        let data = doc.data()
        let act = new ActDocument({
          id: doc.id,
          title: data.title,
          file: data.file,
          status: data.status
        })
        acts.push(act)
      })
      return acts
    })
  }

  async addToFavorites () {
    let uid = firebase.auth().currentUser.uid
    let actRef = actsCollection.doc(this.id)
    let userRef = usersCollection.doc(firebase.auth().currentUser.uid)
    let favoritesRef = usersCollection.doc(uid).collection('favorites').doc(this.id)
    return actRef.get().then(act => {
      return userRef.get().then(user => {
        return favoritesRef.set(act.data()).then(() => {
          return favoritesCollection.doc(this.id).set(act.data()).then(() => {
            return favoritesCollection.doc(this.id).collection('users').doc(firebase.auth().currentUser.uid).set(user.data())
          })
        })
      })
    })
  }

  async removeFromFavorites () {
    let uid = firebase.auth().currentUser.uid
    return usersCollection.doc(uid).collection('favorites').doc(this.id).delete().then(() => {
      return favoritesCollection.doc(this.id).collection('users').doc(uid).delete()
    })
  }

  async getLikeCount () {
    return favoritesCollection.doc(this.id).collection('users').get().then(snapshot => {
      var likeCount = 0
      snapshot.forEach(doc => {
        likeCount++
      })
      return likeCount
    })
  }

  async getFavorites () {
    let uid = firebase.auth().currentUser.uid
    return usersCollection.doc(uid).collection('favorites').get().then(snapshot => {
      var favoriteActs = []
      snapshot.forEach(doc => {
        let data = doc.data()
        let favoriteAct = new ActDocument({
          id: doc.id,
          title: data.title,
          file: data.file,
          status: data.status
        })
        favoriteActs.push(favoriteAct)
      })
      return favoriteActs
    })
  }

  async getByID () {
    return actsCollection.doc(this.id).get().then(doc => {
      return new ActDocument({id: this.id, title: doc.data().title, status: doc.data().status, file: doc.data().file})
    })
  }
}

export default ActDocument
