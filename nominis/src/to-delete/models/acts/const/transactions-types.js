// @TODO add to database
export const transactionsType = [
  'action, construction - rénovation',
  'action, construction - rénovation, agricole',
  'agrandissement commercial',
  'agrandissement résidentiel',
  'aménagement commercial',
  'aménagement extérieur',
  'aménagement résidentiel',
  'annulation de vente par jugement',
  'avis de dénonciation de créance propriétaire',
  'avis de mise en garde',
  'avis de mise en garde, agricole',
  'avis de mise en garde, construction',
  'avis de pré-inscription',
  'avis de pré-inscription, demande de justice',
  'avis de pré-inscription, demande de justice, agricole',
  'avis de pré-inscription, demande de justice, construction',
  'avis de pré-inscription d\'une action',
  'avis de pré-inscription d\'une déclaration de simulation',
  'avis de pré-inscription, agricole',
  'avis de pré-inscription, d\'un bien culturel'
]
