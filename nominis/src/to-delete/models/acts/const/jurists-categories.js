export const juristsCategories = [
  'avocat',
  'greffier',
  'huissier',
  'juge',
  'mandataire',
  'mandaté',
  'notaire',
  'requérant',
  'shérif',
  'sous seing privé',
  'syndic'
]
