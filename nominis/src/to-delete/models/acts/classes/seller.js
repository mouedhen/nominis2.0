import AbstractClass from '../../core/abstract-class'

class Seller extends AbstractClass {
  constructor ({category, name}) {
    super()
    this.category = category
    this.name = name
  }
}

export default Seller
