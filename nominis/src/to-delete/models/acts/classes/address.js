import AbstractClass from '../../core/abstract-class'

class Address extends AbstractClass {
  constructor ({lotNumber, district, address, zipCode, categories}) {
    super()
    this.lotNumber = lotNumber
    this.district = district
    this.zipCode = zipCode
    if (categories !== undefined) {
      this.categories = categories
    } else {
      this.categories = []
    }
  }
}

export default Address
