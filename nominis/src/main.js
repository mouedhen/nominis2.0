import Vue from 'vue'
import './plugins'
import 'normalize.css/normalize.css'
import './assets/scss/style.scss'

import mapboxgl from 'mapbox-gl'
import router from './router'

import store from './store'
import App from './App.vue'
import { convertTimestamp2DateString, convertTimestamp2String, convertTimestamp2DateTimeString } from './core/helpers'

const mbxGeocoding = require('@mapbox/mapbox-sdk/services/geocoding')
const MapboxGeocoder = mbxGeocoding({ accessToken: 'pk.eyJ1IjoibW91ZWRoZW4iLCJhIjoiY2pqOGp3czRrMm51bzNwcWdjZW9heDh5cSJ9.M7AvNbb1mTE5SnR2SB99dA' })

window.mapboxgl = mapboxgl
window.MapboxGeocoder = MapboxGeocoder
Vue.config.productionTip = false

Vue.filter('convertTimestamp2String', function (value) {
  if (!value) return ''
  if (!value.seconds) return ''
  return convertTimestamp2String(value)
})

Vue.filter('convertTimestamp2DateString', function (value) {
  if (!value) return ''
  if (!value.seconds) return ''
  return convertTimestamp2DateString(value)
})

Vue.filter('convertTimestamp2DateTimeString', function (value) {
  if (!value) return ''
  if (!value.seconds) return ''
  return convertTimestamp2DateTimeString(value)
})

Vue.filter('arrayToString', function (value) {
  if (!value && !Array.isArray(value)) return ''
  let result = ''
  value.forEach(v => {
    result += v + ', '
  })
  return result
})

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
